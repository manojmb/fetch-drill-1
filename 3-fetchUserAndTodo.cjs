const path = require("path");
const { promises } = require("fs");

const { fetchData } = require("./2-fetch.cjs");

// const userAPIUrl = "https://jsonplaceholder.typicode.com/users";
// const todoAPIUrl = "https://jsonplaceholder.typicode.com/todos";



// To fetch users and their todo
const apiUrls = [
    "https://jsonplaceholder.typicode.com/users",
    "https://jsonplaceholder.typicode.com/todos"
];

Promise.all(apiUrls.map(url => fetchData(url)))
    .then((dataArray) => {

        const [userData, todoData] = dataArray;
        const userAndTodo = {};
        userData.forEach((user) => {
            userAndTodo[user.id] = { "name": user.name, "todo": [] };
        });
        todoData.forEach((toDo) => {
            if (userAndTodo[toDo.userId]) {
                userAndTodo[toDo.userId].todo.push(toDo);
            }
        })
        promises.writeFile(path.join(__dirname, "output", "3-allUsersAndTodo.json"), JSON.stringify(userAndTodo, null, 2))
    })
    .catch(error => {
        console.error('Error fetching data from one or more APIs:', error);
    });