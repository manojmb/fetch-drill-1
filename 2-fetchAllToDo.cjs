const path = require("path");
const { promises } = require("fs");

const { fetchData } = require("./2-fetch.cjs");

const todoAPIUrl = "https://jsonplaceholder.typicode.com/todos";

fetchData(todoAPIUrl)
    .then((toDos) => {
        promises.writeFile(path.join(__dirname, "output", "2-allTodos.json"), JSON.stringify(toDos, null, 2))
    })
    .catch((err) => console.error(err))

