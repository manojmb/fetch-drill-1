const path = require("path");
const { promises } = require("fs");

const { fetchData } = require("./2-fetch.cjs");

const usersAPIUrl = "https://jsonplaceholder.typicode.com/users";

fetchData(usersAPIUrl)
    .then((users) => {
        promises.writeFile(path.join(__dirname, "output", "1-allUsers.json"), JSON.stringify(users, null, 2))
    })
    .catch((err) => console.error(err))

