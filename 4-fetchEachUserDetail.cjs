const path = require("path");
const { promises } = require("fs");

const { fetchData } = require("./2-fetch.cjs");

const usersAPIUrl = "https://jsonplaceholder.typicode.com/users";

fetchData(usersAPIUrl)
    .then((userData) => {
        let userId = userData.map((user) => user.id)
        let userDetailsPromises = userId.map((id) =>
            fetchData(`https://jsonplaceholder.typicode.com/users?id=${id}`)
        )
        return Promise.all(userDetailsPromises);
    })
    .then((userDetails) => {
        const filePath = path.join(__dirname, 'output', '4-userDetails.json');
        const jsonData = JSON.stringify(userDetails, null, 2);

        return promises.writeFile(filePath, jsonData, 'utf8');
    })
    .catch((err) => console.error(err))

