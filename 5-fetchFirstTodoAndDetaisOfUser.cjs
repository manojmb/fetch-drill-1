const path = require("path");
const { promises } = require("fs");

const { fetchData } = require("./2-fetch.cjs");

const todoAPIUrl = "https://jsonplaceholder.typicode.com/todos";

fetchData(todoAPIUrl)
    .then((todoData) => {
        userId = todoData[0].userId
        return fetchData(`https://jsonplaceholder.typicode.com/users?id=${userId}`)
    })
    .then((userDetail) => {
        const filePath = path.join(__dirname, 'output', '5-toDoUserDetails.json');
        const jsonData = JSON.stringify(userDetail, null, 2);

        return promises.writeFile(filePath, jsonData, 'utf8');
    })
    .catch((err) => console.error(err))

